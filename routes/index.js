var express = require( 'express' )
  , router  = express.Router()
  , tasks   = require( '../models/tasks' )

// Index
router.get( '/', function ( req, res ) {
  res.render( 'index' )
})

router.get( '/task/:id', function ( req, res ) {
  var id = req.params.id
  tasks.findOne( id, function ( err, task ) {
    res.render( 'task', { title: task.content, task: task } )
  })
})

module.exports = router
