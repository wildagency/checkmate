var express = require( 'express' )
  , router  = express.Router()
  , tasks   = require( '../models/tasks' )

// Get all tasks API
router.get( '/', function ( req, res ) {
  tasks.find( req.query, function ( err, tasks ) {
    res.send( tasks )
  })
})

// Get task API
router.get( '/:id', function ( req, res ) {
  var id = req.params.id
  taks.findOne( id, function ( err, task ) {
    res.send( task )
  })
})

// Update task API
router.patch( '/:id', function ( req, res ) {
  var id = req.params.id
  tasks.update( id, req.body, function ( err, task ) {
    res.send( task )
  })
})

// Delete task API
router.delete( '/:id', function ( req, res ) {
  var id = req.params.id
  tasks.delete( id, function ( err, stat ) {
    res.send( stat )
  })
})

// Create task API
router.post( '/', function ( req, res ) {
  tasks.create( req.body, function ( err, task ) {
    res.send( task )
  })
})

module.exports = router
