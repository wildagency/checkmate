var express     = require( 'express' )
  , app         = express()

  , bodyParser  = require( 'body-parser' )
  , less        = require( 'less-middleware' )

  , routes      = require( './routes/index' )
  , api         = require( './routes/api' )

app.use( bodyParser.json() )
app.use( bodyParser.urlencoded( { extended: true } ) )
app.set( 'view engine', 'jade' )
app.set( 'views', './views' )
app.use( less( './public' ) )
app.use( express.static( 'public' ) )

app.use( '/', routes )
app.use( '/api', api )
app.get( '/elements/*', function ( req, res ) {
  res.render( req.path.slice( 1 ) )
})

var server = app.listen( 3000, function () {
  console.log( 'Example app listening at http://%s:%s', server.address().address, server.address().port )
})
