var mongo     = require( 'mongodb' )
  , Db        = mongo.Db
  , ObjectID  = mongo.ObjectID
  , Server    = mongo.Server
  , server    = new Server( 'localhost', 27017, { auto_reconnect: true } )
  , db        = new Db( 'checkmate', server )

db.open()

exports.find = function ( query, callback ) {
  query.parent_id = query.parent_id ? new ObjectID( query.parent_id ) : undefined
  db.collection( 'tasks', function( err, collection ) {
    collection.find( query ).toArray( function( err, tasks ) {
      callback( err, tasks )
    })
  })
}

exports.findOne = function ( id, callback ) {
  db.collection( 'tasks', function( err, collection ) {
    collection.findOne( { '_id': new ObjectID( id ) }, function( err, task ) {
      callback( err, task )
    })
  })
}

exports.update = function ( id, set, callback ) {
  db.collection( 'tasks', function( err, collection ) {
    collection.updateOne( { '_id': new ObjectID( id ) }, { $set: set }, function( err, task ) {
      callback( err, task )
    })
  })
}

exports.delete = function ( id, callback ) {
  db.collection( 'tasks', function( err, collection ) {
    collection.deleteOne( { '_id': new ObjectID( id ) }, function( err, stat ) {
      callback( err, stat )
    })
  })
}

exports.create = function ( task, callback ) {
  if ( task.parent_id ) {
    task.parent_id = new ObjectID( task.parent_id )
  }
  db.collection( 'tasks', function( err, collection ) {
    collection.insertOne( task, function( err ) {
      callback( err, task )
    })
  })
}
